import React, { useContext, useState, useEffect } from "react";
import Button from "../Button/Button";
import ProductCard from "../ProductCard/ProductCard";
import { ProductCardContext } from "../ProductCard/ProductCardContext";
import Modal from "../Modal/Modal";

const Basket = () => {
	const {
		items,
		card,
		setCard,
		closeModal,
		addToFavorite,
		show,
		openModal,
		favorite,
		onDelete,
		modal,
	} = useContext(ProductCardContext);

	useEffect(() => {
		const newCard = card.map((el) => items.find((item) => item.id === el.id));
		newCard.map((el) => (el.idForDelete = el.id));
		setCard(newCard);
	}, [favorite]);

	return (
		<>
			<ul className="product-cards">
				{card.map((e) => (
					<ProductCard
						id={e.id}
						key={e.id}
						name={e.name}
						price={e.Price}
						routeImg={e.routeImg}
						color={e.color}
						onClick={() => addToFavorite(e)}
						className="favor-icon"
						isFavorite={e.isFavorite}
						actions={
							<>
								<Button
									id={e.id}
									// modalId={e.id}
									className="btn"
									onClick={() => openModal(e.id)}
									text="Удалить из корзины"
								/>
							</>
						}
					/>
				))}
			</ul>
			{show && (
				<Modal
					onClick={closeModal}
					header="Удалить товар из корзины?"
					actions={
						<>
							<Button
								// modalId={modal.id}
								btnClassName="button apply"
								onClick={onDelete(modal.id)}
								className="button"
								text="Удалить из корзины"
							/>
							<Button
								btnClassName="button confirm"
								// onClick={closeModal}
								className="button"
								text="Купить"
							/>
						</>
					}
				/>
			)}
		</>
	);
};

export default Basket;
