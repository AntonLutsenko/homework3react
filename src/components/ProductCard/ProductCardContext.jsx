import React from "react";

export const ProductCardContext = React.createContext({
	items: [],
	card: [],
	setCard: () => {},
	// clearCard: () => {},
	onDelete: () => {},
	addToCard: () => {},
	closeModal: () => {},
	addToFavorite: () => {},
	setItems: () => {},
	show: "",
	modal: "",
	openModal: () => {},
	favorite: [],
});
