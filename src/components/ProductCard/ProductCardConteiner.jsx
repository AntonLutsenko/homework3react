import React, { useState, useEffect } from "react";
import { ProductCardContext } from "./ProductCardContext";
import axios from "axios";

const ProductCardConteiner = (props) => {
	const [items, setItems] = useState([]);
	const [card, setCard] = useState([]);
	const [show, showModal] = useState(false);
	const [modal, modalForm] = useState();
	const [favorite, setFavorite] = useState([]);

	useEffect(() => {
		axios("/data.json").then((res) => {
			setItems(res.data);
			if (!localStorage.getItem("card")) {
				localStorage.setItem("card", "[]");
			} else {
				setCard(JSON.parse(localStorage["card"]));
			}
		});

		if (localStorage.getItem("favorites") !== null) {
			setFavorite(JSON.parse(localStorage["favorites"]));
		}
	}, []);

	useEffect(() => {
		const newItems = items.map((element) => {
			favorite.includes(element)
				? (element.isFavorite = true)
				: (element.isFavorite = false);
			return element;
		});

		setItems(newItems);
	}, [favorite, card]);

	const addToFavorite = (element) => {
		if (favorite.includes(element)) {
			const newFavorites = favorite.filter((item) => item !== element);
			setFavorite(newFavorites);
		} else {
			setFavorite((arr) => [...arr, element]);
		}
	};

	useEffect(() => {
		localStorage.setItem("favorites", JSON.stringify(favorite));
	}, [favorite]);

	const openModal = (id) => {
		showModal(true);
		const dataForModal = card.find((item) => item.id === id);
		modalForm(dataForModal);
	};

	const closeModal = () => {
		showModal(false);
	};

	const addToCard = (id) => {
		showModal(true);
		let dataForModal = items.find((item) => item.id === id);
		modalForm((dataForModal = { ...dataForModal, idForDelete: id }));
		card.push(dataForModal);
		localStorage.setItem("card", JSON.stringify(card));
	};

	const hendleDelete = (id) => {
		const newCard = card.filter((element) => {
			if (element.idForDelete === id) {
				return false;
			} else {
				return true;
			}
		});
		setCard(newCard);
		localStorage.setItem("card", JSON.stringify(newCard));
		closeModal();
	};

	const { children } = props;

	return (
		<ProductCardContext.Provider
			value={{
				items,
				card: card,
				setItems: setItems,
				setCard: setCard,
				onDelete: hendleDelete,
				addToCard: addToCard,
				closeModal: closeModal,
				addToFavorite: addToFavorite,
				openModal: openModal,
				show,
				modal,
				favorite,
			}}
		>
			{children}
		</ProductCardContext.Provider>
	);
};

export default ProductCardConteiner;
