import React, { useContext, useState, useEffect } from "react";
import Button from "../Button/Button";
import ProductCard from "../ProductCard/ProductCard";
import { ProductCardContext } from "../ProductCard/ProductCardContext";
import Modal from "../Modal/Modal";

const Favorites = () => {
	const { addToCard, closeModal, addToFavorite, show, modal, favorite } =
		useContext(ProductCardContext);

	return (
		<>
			<ul className="product-cards">
				{favorite.map((e) => (
					<ProductCard
						id={e.id}
						key={e.id}
						name={e.name}
						price={e.Price}
						routeImg={e.routeImg}
						color={e.color}
						onClick={() => addToFavorite(e)}
						className="favor-icon"
						isFavorite={e.isFavorite}
						actions={
							<>
								<Button
									id={e.id}
									modalId={e.id}
									className="btn"
									onClick={() => addToCard(e.id)}
									text="Добавить в корзину"
								/>
							</>
						}
					/>
				))}
			</ul>
			{show && (
				<Modal
					onClick={closeModal}
					header="Удалить товар из корзины?"
					// text={}
					// header={}
					// background:
					// id={modal.id}
					actions={
						<>
							<Button
								modalId={modal.id}
								btnClassName="button apply"
								// onClick={onDelete(modal.id)}
								className="button"
								text="Добавить в корзину"
							/>
							<Button
								btnClassName="button confirm"
								// onClick={closeModal}
								className="button"
								text="Продолжить покупку"
							/>
						</>
					}
				/>
			)}
		</>
	);
};

export default Favorites;
