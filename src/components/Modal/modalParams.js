const modalParams = [
	{
		"data-Id": "first",
		header: "Добавить данный продукт в корзину?",
		text: `Данный телефон будет добавлен в корзину покупок`,
		backgroundColor: "rgb(15 193 74 / 90%)",
		closeButton: true,
		action1: "Ok",
		action2: "Cancel",
		classNameBtn1: "button btn-ok",
		classNameBtn2: "button btn-cancel",
	},
	{
		"data-Id": "second",
		header: "Вы уверены, что хотите удалить данный продукт из корзины?",
		text: "товар будет удален из корзины покупок",

		backgroundColor: "rgba(76, 177, 76, 0.8)",
		closeButton: true,
		action1: "Удалить",
		action2: "Отменить",
		classNameBtn1: "button btn-ok-second",
		classNameBtn2: "button btn-cancel-second",
	},
];

export default modalParams;
