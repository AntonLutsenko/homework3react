import React from "react";
import "./Modal.scss";

const Modal = (props) => {
	return (
		<>
			<div className="modal-container" onClick={props.onClick}>
				<div
					className="modal-content"
					style={{ backgroundColor: props.backgroundColor }}
				>
					<div className="modal-content__header">
						{props.header}
						{props.closeButton && (
							<span
								className="modal-content__close-btn"
								onClick={props.onClick}
							></span>
						)}
					</div>
					<p className="modal-content__text">{props.text}</p>
					<span className="modal-content__span">{props.text2}</span>
					<div className="btn-container">{props.actions}</div>
				</div>
			</div>
		</>
	);
};

export default Modal;

Modal.defaultProps = {
	header: "Добавить товар в корзину?",
	closeButton: "true",
	backgroundColor: "rgba(15, 150, 50, 0.7)",
};
