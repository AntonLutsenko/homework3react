import React, { useContext, useEffect } from "react";
import ProductCard from "../ProductCard/ProductCard";
import { ProductCardContext } from "../ProductCard/ProductCardContext";
import "./productList.scss";
import Modal from "../Modal/Modal";
import Button from "../Button/Button";
import { useNavigate } from "react-router-dom";

const ProductList = () => {
	const navigate = useNavigate();

	const {
		items,
		addToCard,
		closeModal,
		addToFavorite,
		show,
		modal,
		favorite,
		setItems,
	} = useContext(ProductCardContext);

	const moveToBasket = (e) => {
		navigate("/basket");
		closeModal();
	};

	return (
		<>
			<ul className="product-cards">
				{items.map((e) => (
					<ProductCard
						id={e.id}
						key={e.id}
						name={e.name}
						price={e.Price}
						routeImg={e.routeImg}
						color={e.color}
						onClick={() => addToFavorite(e)}
						className="favor-icon"
						isFavorite={e.isFavorite}
						actions={
							<>
								<Button
									id={e.id}
									modalId={e.id}
									className="btn"
									onClick={() => addToCard(e.id)}
									text="Добавить в корзину"
								/>
							</>
						}
					/>
				))}
			</ul>
			{show && (
				<Modal
					onClick={closeModal}
					// text={}
					// header={}
					// background:
					id={modal.id}
					// imgRoute={modal.imgRoute}
					// name={modal.name}
					// price={modal.price}
					actions={
						<>
							<Button
								btnClassName="button apply"
								onClick={moveToBasket}
								className="button"
								text="перейти в корзину"
							/>
							<Button
								btnClassName="button confirm"
								onClick={closeModal}
								className="button"
								text="продолжить покупки"
							/>
						</>
					}
				/>
			)}
		</>
	);
};

export default ProductList;
