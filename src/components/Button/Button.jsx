import React from "react";
import "./Button.scss";

const Button = ({ dataid, className, backgroundColor, onClick, text, id }) => {
	return (
		<button
			id={id}
			dataid={dataid}
			className={className}
			style={{ backgroundColor: backgroundColor }}
			onClick={onClick}
			// modalId={modalId}
		>
			{text}
		</button>
	);
};

export default Button;
