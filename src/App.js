import "./Reset.css";
import "./App.scss";
import ProductList from "./components/ProductList/ProductList";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import ProductCardConteiner from "./components/ProductCard/ProductCardConteiner";
import Header from "./components/Header/Header";
import Basket from "./components/Basket/Basket";
import Favorites from "./components/Favorites/Favorites";

const App = () => {
	return (
		<BrowserRouter>
			<ProductCardConteiner>
				<Header />
				<Routes>
					<Route path="/" element={<ProductList />}></Route>
					<Route path="basket" element={<Basket />}></Route>
					<Route path="favorites" element={<Favorites />}></Route>
				</Routes>
			</ProductCardConteiner>
		</BrowserRouter>
	);
};

export default App;
